set number

noremap <C-e> $a
inoremap <C-e> <Esc>$a
noremap <C-a> ^i
inoremap <C-a> <Esc>^i

inoremap " ""<Left>
inoremap ' ''<Left>
inoremap { {}<Left>
inoremap [ []<Left>
inoremap ( ()<Left>
inoremap {<CR> {<CR>}<Esc>k$a<CR>

"dein Scripts-----------------------------

if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
execute "set runtimepath+=".$HOME."/.cache/dein/repos/github.com/Shougo/dein.vim"

" Required:
call dein#begin($HONE.'/.cache/dein')

" Let dein manage dein
" Required:
call dein#add($HOME.'/.cache/dein/repos/github.com/Shougo/dein.vim')

" Add or remove your plugins here like this:
call dein#add('prabirshrestha/vim-lsp')
call dein#add('mattn/vim-lsp-settings')
call dein#add('prabirshrestha/asyncomplete.vim')
call dein#add('prabirshrestha/asyncomplete-lsp.vim')

"call dein#add('Shougo/neosnippet.vim')
"call dein#add('Shougo/neosnippet-snippets')

" Required:
call dein#end()

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
"if dein#check_install()
"  call dein#install()
"endif

"End dein Scripts-------------------------

